Specifications
==============

The GNOME platform depends on various specifications to allow interoperability
between applications, platforms, and environments:

* `Autostart <https://www.freedesktop.org/wiki/Specifications/autostart-spec/>`_
* `Base directories <https://www.freedesktop.org/wiki/Specifications/basedir-spec/>`_ 
* `Desktop entries <https://www.freedesktop.org/wiki/Specifications/desktop-entry-spec/>`_
* `Icon themes <https://www.freedesktop.org/wiki/Specifications/icon-theme-spec/>`_
* `Shared MIME database <https://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/>`_
* `Trash storage <https://www.freedesktop.org/wiki/Specifications/trash-spec/>`_

For more information, see the `freedesktop.org project
<https://freedesktop.org>`_.

Additionally, GNOME depends on file formats and IPC interfaces that are
available for the purpose of interoperability:

* :doc:`GVariant <specifications/gvariant-specification-1.0>`

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Contents

   specifications/gvariant-specification-1.0
