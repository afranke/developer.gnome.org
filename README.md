# Developer Documentation

This is the source for the [GNOME Developer documentation website][dgo].

The content is written using [reStructuredText][rst], generated using
[Sphinx][sphinx], and hosted using [GitLab Pages][gitlab-pages] on the GNOME
infrastructure.

## How to edit the documentation

You can add, remove, and modify the content by editing the `.rst` files
under sources, using your preferred text editor.

## How to build the documentation locally

You will need Python 3.

You will need to install Sphinx and other dependencies listed in the
[`requirements.txt`](./requirements.txt) file:

```
pip install --user --upgrade -r requirements.txt
```

Once you have all the dependencies installed, you can run:

```
./build.sh
```

from the project's root. The build output will be in the `build` directory.

You can use:

```
./build.sh --incremental
```

For faster, incremental builds.

## Deployment

All changes applied to the `main` branch will automatically be deployed
through the CI pipeline.

[dgo]: https://developer.gnome.org
[rst]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
[sphinx]: https://www.sphinx-doc.org/en/master/index.html
[gitlab-pages]: https://docs.gitlab.com/ee/user/project/pages/
